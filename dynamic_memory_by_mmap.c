#include <stdio.h>
#include <unistd.h> // getpagesize
#include <string.h> // memset
#include <sys/mman.h> // mmap

int main(void)
{
	void *buf;
	int buf_size = getpagesize() * 1024;
	printf("Attempting to dynamically allocates %d bytes memory using mmap(2).", buf_size);
	getchar();
        buf = mmap(NULL, buf_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (buf == MAP_FAILED)
		perror("mmap failed:");
	else
		printf("Success dynamically allocatation of %d bytes memory using mmap(2).", buf_size);
	memset(buf, 0, buf_size);
	getchar();
}
